function formatDate(dateString) {
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}/${month}/${day}`;
  }
  
function createCard(name, description, pictureUrl, starts, ends, location) {
    const formattedStarts = formatDate(starts);
    const formattedEnds = formatDate(ends);
    
    return `
     <div class="shadow mb-5 bg-body-tertiary rounded">
        <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            </div>
            <div class="card footer">${formattedStarts} - ${formattedEnds}</div>
        </div>
     </div>
    `;
  }
  
  createAlert()

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        console.log('response is bad')
      } else {
        const data = await response.json();
        
        const container = document.querySelector('.row');
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const starts = details.conference.starts
            const ends = details.conference.ends
            const location = details.conference.location.name
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(name, description, pictureUrl, starts, ends, location);
            
            const cardElement = document.createElement('div');
            cardElement.classList.add('col');
            cardElement.innerHTML = html;
            container.append(cardElement);
          }
        }
  
      }
    } catch (e) {
        console.error(e)
        console.log('ERROR')
    }
  
  });
